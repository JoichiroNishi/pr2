//The DB of the doctors in the polyclinic (their schedule)
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int scanPoint();
void AddInfo();
void DeleteInfo();
void ChangeInfo();


//scan a point of menu
int ScanPoint()
{
	int choosePoint;
	scanf("%i", &choosePoint);
	return choosePoint;
}


//adding info in the main table (employing of doctors)
void AddInfo()
{
	char date[10], startTime[5], finishTime[5], typeOfReception[9], lastNamePat[30], firstNamePat[20], lastNameDoc[30], firstNameDoc[20];
	int typeOfRec;
	printf("-------------------ADD INFO-------------------\n");
	printf("Enter a date of reception in dd/mm/yyyy format: ");
	scanf("%s", date);
	printf("Enter a time of reception's start in hh:mm format: ");
	scanf("%s", startTime);
	printf("Enter a time of reception's finish in hh:mm format: ");
	scanf("%s", finishTime);
	printf("Choose a type of the reception:\n");
	printf("1 primary\n");
	printf("2 secondary\n");
	printf("Enter: ");
	scanf("%i", &typeOfRec);
	switch(typeOfRec)
	{
		case 1: strcpy(typeOfReception, "primary");
			break;
		case 2: strcpy(typeOfReception, "secondary");
			break;
	}
	printf("Enter the last name of a patient: ");
	scanf("%s", lastNamePat);
	printf("Enter the first name of a patient: ");
	scanf("%s", firstNamePat);
	printf("Enter the last name of a doctor: ");
	scanf("%s", lastNameDoc);
	printf("Enter the first name of a doctor: ");
	scanf("%s", firstNameDoc);
}

//deleting info in the main table (employing of doctors)
void DeleteInfo()
{
	printf("-------------------DELETE INFO-------------------\n");
}

//changing info in the main table (employing of doctors)
void ChangeInfo()
{
	printf("-------------------CHANGE INFO-------------------\n");
}

void MainMenu()
{
	const int pointsMaxNum = 3;
	int choosePoint;
	printf("Welcome to the Database Polyclinic!\n");
	printf("Choose one of the points below to continue\n");
	printf("1 Add info\n");
	printf("2 Delete info\n");
	printf("3 Change info\n");
	printf("4 Exit\n");
	choosePoint = ScanPoint();
	switch(choosePoint)
	{
		case 1: AddInfo();
			break;
		case 2: DeleteInfo();
			break;
		case 3: ChangeInfo();
			break;
		case 4: exit(0);
			break;
	}
	
}

int main(void) 
{
	MainMenu();
	return 0;
}
