//The DB of the doctors in the polyclinic (their schedule)
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

//int scanPoint();
//void AddInfo();
//void DeleteInfo();
//void ChangeInfo();

//scan a point of menu
int ScanPoint()
{
	int choosePoint;
	scanf("%i", &choosePoint);
	return choosePoint;
}

//adding info in the main table (employing of doctors)
void AddInfo()
{
	char date[10], startTime[5], finishTime[5], lastNamePat[30], firstNamePat[20], lastNameDoc[30], firstNameDoc[20];
//	const char primary = "primary", secondary = "secondary";
	int typeOfRec;
	printf("-------------------ADD INFO-------------------\n");
	printf("Enter a date of reception in dd/mm/yyyy format: ");
	scanf("%9s", date);
	printf("Enter a time of reception's start in hh:mm format: ");
	scanf("%4s", startTime);
	printf("Enter a time of reception's finish in hh:mm format: ");
	scanf("%4s", finishTime);
	printf("Choose a type of the reception:\n");
	printf("1 primary\n");
	printf("2 secondary\n");
	printf("Enter: ");
	scanf("%0i", &typeOfRec);//-> CHECK IT LATER
	printf("Enter the last name of a patient: ");
	scanf("%29s", lastNamePat);
	printf("Enter the first name of a patient: ");
	scanf("%19s", firstNamePat);
	printf("Enter the last name of a doctor: ");
	scanf("%29s", lastNameDoc);
	printf("Enter the first name of a doctor: ");
	scanf("%19s", firstNameDoc);
}

//deleting info in the main table (employing of doctors)
void DeleteInfo()
{
	printf("-------------------DELETE INFO-------------------\n");
}

//changing info in the main table (employing of doctors)
void ChangeInfo()
{
	printf("-------------------CHANGE INFO-------------------\n");
}

