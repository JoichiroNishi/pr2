//The DB of the doctors in the polyclinic (their schedule)
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void MainMenu()
{
	int choosePoint;
	printf("Welcome to the Database Polyclinic!\n");
	printf("Choose one of the points below to continue\n");
	printf("1 Add info\n");
	printf("2 Delete info\n");
	printf("3 Change info\n");
	printf("4 Exit\n");
	choosePoint = ScanPoint();
	switch(choosePoint)
	{
		case 1: AddInfo();
			break;
		case 2: DeleteInfo();
			break;
		case 3: ChangeInfo();
			break;
		case 4: exit(0);
			break;
	}
}
